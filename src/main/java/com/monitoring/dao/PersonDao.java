package com.monitoring.dao;

import com.monitoring.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface PersonDao extends JpaRepository<Person, Integer> {

    long countByCreatedDate(LocalDate createdDate);

    @Query(value = "SELECT SUM(amount_of_money) FROM person WHERE created_date = ?1", nativeQuery = true)
    long getAmountOfMoney(LocalDate date);
}
