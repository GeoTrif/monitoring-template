package com.monitoring.restcontroller;

import com.monitoring.model.MonitoringOutput;
import com.monitoring.service.MonitoringService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MonitoringServiceRestController {

    private final MonitoringService monitoringService;

    public MonitoringServiceRestController(MonitoringService monitoringService) {
        this.monitoringService = monitoringService;
    }

    @GetMapping("/get-persons")
    public long getPersonsCount() {
        return monitoringService.getPersonsCount();
    }

    @GetMapping("/monitoring-output")
    public MonitoringOutput getMonitpringOutput() {
        return monitoringService.getMonitoringOutput();
    }
}
