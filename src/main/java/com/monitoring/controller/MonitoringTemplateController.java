package com.monitoring.controller;

import com.monitoring.service.MonitoringService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/monitoring-template")
public class MonitoringTemplateController {

    private static final String MONITORING_TEMPLATE_VIEW = "monitoring-template";
    private static final String NUMBER_OF_PERSONS_ATTRIBUTE_NAME = "numberOfPersons";
    private static final String MONITORING_OUTPUT_ATTRIBUTE_NAME = "monitoringOutput";

    private final MonitoringService monitoringService;

    public MonitoringTemplateController(MonitoringService monitoringService) {
        this.monitoringService = monitoringService;
    }

    @GetMapping
    public String getMonitoringTemplatePage(Model model) {
        model.addAttribute(NUMBER_OF_PERSONS_ATTRIBUTE_NAME, monitoringService.getPersonsCount());
        model.addAttribute(MONITORING_OUTPUT_ATTRIBUTE_NAME, monitoringService.getMonitoringOutput());

        return MONITORING_TEMPLATE_VIEW;
    }
}
