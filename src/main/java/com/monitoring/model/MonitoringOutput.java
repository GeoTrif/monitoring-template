package com.monitoring.model;

public class MonitoringOutput {

    private long personCount;
    private long amountOfMoneyCount;

    public MonitoringOutput() {
    }

    public MonitoringOutput(long personCount, long amountOfMoneyCount) {
        this.personCount = personCount;
        this.amountOfMoneyCount = amountOfMoneyCount;
    }

    public long getPersonCount() {
        return personCount;
    }

    public void setPersonCount(long personCount) {
        this.personCount = personCount;
    }

    public long getAmountOfMoneyCount() {
        return amountOfMoneyCount;
    }

    public void setAmountOfMoneyCount(long amountOfMoneyCount) {
        this.amountOfMoneyCount = amountOfMoneyCount;
    }

    @Override
    public String toString() {
        return "MonitoringOutput{" +
                "personCount=" + personCount +
                ", amountOfMoneyCount=" + amountOfMoneyCount +
                '}';
    }
}
