package com.monitoring.service;

import com.monitoring.dao.PersonDao;
import com.monitoring.model.MonitoringOutput;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class MonitoringService {

    private final PersonDao personDao;
    private final AlertService alertService;

    private long personsCount;
    private final MonitoringOutput monitoringOutput = new MonitoringOutput();

    public MonitoringService(final PersonDao personDao, final AlertService alertService) {
        this.personDao = personDao;
        this.alertService = alertService;
    }

    public long getPersonsCount() {
        return personsCount;
    }

    public MonitoringOutput getMonitoringOutput() {
        return monitoringOutput;
    }

    @Scheduled(cron = "${monitoring.template.schedule}")
    private void updatePersonsCount() {
        // Use proper logging instead of prints.
        System.out.println("Old persons count: " + personsCount);
        personsCount = personDao.countByCreatedDate(LocalDate.now());

        System.out.println("New persons count: " + personsCount);
    }

    @Scheduled(cron = "${monitoring.template.schedule}")
    private void updateMonitoringOutput() {
        System.out.println("Old monitoring output: " + monitoringOutput);

        monitoringOutput.setPersonCount(personDao.countByCreatedDate(LocalDate.now()));
        monitoringOutput.setAmountOfMoneyCount(personDao.getAmountOfMoney(LocalDate.now()));

        System.out.println("New monitoring output: " + monitoringOutput);
    }

    @Scheduled(cron = "${monitoring.template.schedule.alert}")
    private void alertViaEmail() {
        if (personsCount == 0 || (monitoringOutput.getPersonCount() == 0 || monitoringOutput.getAmountOfMoneyCount() == 0)) {
            alertService.sendAlertToMail(personsCount, monitoringOutput);
        }
    }
}
