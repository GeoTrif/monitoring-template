package com.monitoring.service;

import com.monitoring.model.MonitoringOutput;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class AlertService {

    private static final String MAIL_TEXT_BODY = "Number of persons: %d\nAmount of money: %s";
    private static final String SET_FROM = "alerts@monitoring.com";
    private static final String SET_TO = "your_mail@gmail.com";
    private static final String SUBJECT = "Alert";

    private final JavaMailSender javaMailSender;

    public AlertService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    /**
     * In order to properly send an alert to gmail, you need to go to gmail all settings, 'Account and Import' tab.
     * Select 'Other Google Account Settings'.Go to 'Security' tab.
     * Turn off the two way authentication,and after you did this you also need to turn on the 'Less Secure Apps' config.
     * After these steps,if you properly configured your JavaMailSender, you should see an 'Alert' email in the 'Sent To' email address.
     *
     * @param personsCount
     * @param monitoringOutput
     */
    public void sendAlertToMail(long personsCount, MonitoringOutput monitoringOutput) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(SET_FROM);
        message.setTo(SET_TO);

        message.setSubject(SUBJECT);
        message.setText(String.format(MAIL_TEXT_BODY, personsCount, monitoringOutput));

        javaMailSender.send(message);
    }
}
